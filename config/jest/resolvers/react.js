function react(result = {}) {
  return {
    ...result,
    '^@/(.*)$': '<rootDir>/src/$1',
    '^@assets(.*)$': '<rootDir>/src/assets$1',
    '^@components(.*)$': '<rootDir>/src/components$1',
    '^@constants(.*)$': '<rootDir>/src/constants$1',
    '^@context(.*)$': '<rootDir>/src/context$1',
    '^@helpers(.*)$': '<rootDir>/src/helpers$1',
    '^@hooks(.*)$': '<rootDir>/src/hooks$1',
    '^@styles(.*)$': '<rootDir>/src/styles$1',
    '^@route-gateway(.*)$': '<rootDir>/src/routes/index.js',
    '^@routes(.*)$': '<rootDir>/src/routes$1',
    '^@config(.*)$': '<rootDir>/config$1',
    '^@stores(.*)$': '<rootDir>/src/stores$1',
    '^@worker(.*)$': '<rootDir>/src/serviceWorker/index.js',
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  };
}

module.exports = react;
