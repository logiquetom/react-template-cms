module.exports = {
  presets: ['react-app'],
  plugins: ['lodash', '@babel/plugin-proposal-optional-chaining', '@babel/plugin-proposal-nullish-coalescing-operator'],
};
