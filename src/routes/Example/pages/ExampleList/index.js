import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Card } from '@metronic/_partials/controls/Card.js';
import { useSubheader } from '@metronic/layout';
import LoadingSkeleton from '@components/LoadingSkeleton';
import Snackbar from '@components/Snackbar';
import { ToasterContext } from '@context/toaster';
import AlertDialog from '@components/AlertDialog';
import { Textfield } from '@components/Textfield';

import { useTableFilter } from '@components/DataTable/useTableFilter';
import { DataTable, TableFilter } from '@components/DataTable';
import TablePagination from '@material-ui/core/TablePagination';

const ExampleList = () => {
  const { showSnackbar } = useContext(ToasterContext);
  const subheader = useSubheader();
  useEffect(() => {
    subheader.setTitle('Example');
    subheader.setBreadcrumbs([
      {
        pathname: '/example',
        title: 'Example List',
      },
    ]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //Handle Fetch
  const { state: filterState, onSearch, onSort, changePage, changePageSize, onClear } = useTableFilter();
  const { search, sort, page, pageSize } = filterState;

  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
    return () => {
      clearTimeout();
    };
  }, []);
  const tableListDummy = [
    { id: '1', no: '1', title: 'example1' },
    { id: '2', no: '2', title: 'example2' },
    { id: '3', no: '3', title: 'example3' },
    { id: '4', no: '4', title: 'example4' },
    { id: '5', no: '5', title: 'example5' },
    { id: '6', no: '6', title: 'example6' },
    { id: '7', no: '7', title: 'example7' },
    { id: '8', no: '8', title: 'example8' },
    { id: '9', no: '9', title: 'example9' },
    { id: '10', no: '10', title: 'example10' },
    { id: '11', no: '11', title: 'example11' },
    { id: '12', no: '12', title: 'example12' },
  ];

  const pagination = {
    page: 1,
    total: 12,
  };

  const sortFunction = (columnName, sortOrder) => {
    onSort({
      columnName: columnName,
      sortOrder: sortOrder,
    });
  };

  const handleChangePage = (event, newPage) => {
    changePage(newPage);
  };

  const handleChangePageSize = event => {
    changePageSize(parseInt(event.target.value, 10));
  };
  //---------------------------------------------------

  //Handle Delete
  const [alertDialog, setAlertDialog] = useState({ display: false, targetID: null });
  const [deleteLoading, setDeleteLoading] = useState(false);
  const deleteRow = row => {
    setAlertDialog({ display: true, targetID: row.id });
  };

  const onConfirmDelete = () => {
    setDeleteLoading(true);
    console.log('Target delete ID', alertDialog.targetID);
  };

  const moreFilters = [
    <Textfield className="mr-3" key="1" name="123" value="123" placeholder="filter by 123" label="filter" />,
  ];
  //------------------------------------------------------

  return (
    <div className="styContainer">
      <div className="d-flex my-5">
        <Link className="btn btn-dark btn-lg mr-3" to="/example/create">
          Add Example
        </Link>
        <div
          className="btn btn-dark btn-lg"
          onClick={() =>
            showSnackbar({
              text: 'Test snackbar',
              timeout: 5000,
              variant: 'success',
            })
          }
        >
          Show Snackbar
        </div>
      </div>
      <Card className="p-5 mb-5">
        <TableFilter search={search} onSearch={onSearch} onClear={onClear} moreFilters={moreFilters} />
      </Card>
      <Card className="py-5">
        {loading ? (
          <LoadingSkeleton className="px-3" count={3} />
        ) : (
          <>
            <DataTable
              isFetching={loading}
              rows={tableListDummy}
              updateLink="/example/update/"
              deleteRow={deleteRow}
              headers={[
                { displayName: 'No', key: 'no' },
                { displayName: 'Title', key: 'title' },
              ]}
              sortByDirection={sort.sortOrder}
              sortByColumnName={sort.columName}
              sortFunction={sortFunction}
              Pagination={
                <TablePagination
                  component="div"
                  count={pagination?.total}
                  page={page}
                  rowsPerPageOptions={[10, 25]}
                  onChangePage={handleChangePage}
                  rowsPerPage={pageSize}
                  onChangeRowsPerPage={handleChangePageSize}
                />
              }
            />
          </>
        )}
      </Card>
      <Snackbar />
      <AlertDialog
        action="Delete"
        title="Delete Make"
        description={`Are you sure you want to delete ID #${alertDialog.targetID}`}
        display={alertDialog.display}
        onConfirm={onConfirmDelete}
        closeDialog={() => setAlertDialog({ display: false, targetID: null })}
        loading={deleteLoading}
      />
    </div>
  );
};
export default ExampleList;
