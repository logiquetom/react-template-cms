import React from 'react';
import { ContentRoute } from '@metronic/layout';
import ExampleList from '@routes/Example/pages/ExampleList';
import ExampleCreate from '@routes/Example/pages/ExampleCreate';
import ExampleUpdate from '@routes/Example/pages/ExampleUpdate';

export default () => {
  return (
    <>
      <ContentRoute exact path="/example" component={ExampleList} />
      <ContentRoute exact path="/example/create" component={ExampleCreate} />
      <ContentRoute exact path="/example/update/:id" component={ExampleUpdate} />
    </>
  );
};
