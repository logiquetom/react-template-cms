import React, { Suspense, lazy } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { Layout, LayoutSplashScreen, ContentRoute } from '@metronic/layout';
// import { Logout, AuthPage } from '@metronic/modules/Auth';
import ErrorsPage from '@metronic/modules/ErrorsExamples/ErrorsPage';
import { BuilderPage } from '@metronic/pages/BuilderPage';
import { MyPage } from '@metronic/pages/MyPage';
import { DashboardPage } from '@metronic/pages/DashboardPage';
import { Logout, AuthPage } from '@routes/Auth';

const GoogleMaterialPage = lazy(() => import('@metronic/modules/GoogleMaterialExamples/GoogleMaterialPage'));
const ReactBootstrapPage = lazy(() => import('@metronic/modules/ReactBootstrapExamples/ReactBootstrapPage'));
const ECommercePage = lazy(() => import('@metronic/modules/ECommerce/pages/eCommercePage'));
const Example = lazy(() => import('@routes/Example'));

const BasePage = () => {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <Redirect exact from="/" to="/dashboard" />
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/my-page" component={MyPage} />

        <ContentRoute path="/example" component={Example} />

        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/e-commerce" component={ECommercePage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
};

export default function Routes() {
  const isAuthorized = true;

  return (
    <Switch>
      {!isAuthorized ? <AuthPage /> : <Redirect from="/auth" to="/" />}

      <Route path="/error" component={ErrorsPage} />
      <Route path="/logout" component={Logout} />

      {!isAuthorized ? (
        /*Redirect to `/auth` when user is not authorized*/
        <Redirect to="/auth/login" />
      ) : (
        <Layout>
          <BasePage />
        </Layout>
      )}
    </Switch>
  );
}
