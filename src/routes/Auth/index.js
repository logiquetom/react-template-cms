import React from 'react';
import loadable from '@loadable/component';
import SplashScreen from '@metronic/_partials/controls/SplashScreen';

export const AuthPage = loadable(() => import(/* webpackChunkName: "auth-page-container" */ './pages/AuthPage'), {
  fallback: <SplashScreen />,
});

export { default as Logout } from './pages/Logout';
