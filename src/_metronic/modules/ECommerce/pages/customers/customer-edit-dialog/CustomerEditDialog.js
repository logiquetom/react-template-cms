import React, { useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import { CustomerEditDialogHeader } from './CustomerEditDialogHeader';
import { CustomerEditForm } from './CustomerEditForm';
import { useCustomersUIContext } from '../CustomersUIContext';

export function CustomerEditDialog({ id, show, onHide }) {
  // Customers UI Context
  const customersUIContext = useCustomersUIContext();
  const customersUIProps = useMemo(() => {
    return {
      initCustomer: customersUIContext.initCustomer,
    };
  }, [customersUIContext]);

  return (
    <Modal size="lg" show={show} onHide={onHide} aria-labelledby="example-modal-sizes-title-lg">
      <CustomerEditDialogHeader id={id} />
      <CustomerEditForm
        saveCustomer={() => {}}
        actionsLoading={false}
        customer={customersUIProps.initCustomer}
        onHide={onHide}
      />
    </Modal>
  );
}
