import React from 'react';
import { Modal } from 'react-bootstrap';

export function CustomerEditDialogHeader({ id }) {
  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">Edit Customer</Modal.Title>
      </Modal.Header>
    </>
  );
}
