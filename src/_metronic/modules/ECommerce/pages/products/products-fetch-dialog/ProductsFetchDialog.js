import React, { useEffect, useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import { ProductStatusCssClasses } from '../ProductsUIHelpers';
import { useProductsUIContext } from '../ProductsUIContext';

const products = [
  {
    id: 226,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 1,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 20982196,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 256,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 23126,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 216,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
];

export function ProductsFetchDialog({ show, onHide }) {
  // Products UI Context
  const productsUIContext = useProductsUIContext();
  const productsUIProps = useMemo(() => {
    return {
      ids: productsUIContext.ids,
      queryParams: productsUIContext.queryParams,
    };
  }, [productsUIContext]);

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!productsUIProps.ids || productsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productsUIProps.ids]);

  return (
    <Modal show={show} onHide={onHide} aria-labelledby="example-modal-sizes-title-lg">
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">Fetch selected elements</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {products.map(product => (
              <div className="list-timeline-item mb-3" key={product.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${ProductStatusCssClasses[product.status]} label-inline`}
                    style={{ width: '60px' }}
                  >
                    ID: {product.id}
                  </span>{' '}
                  <span className="ml-5">
                    {product.manufacture}, {product.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button type="button" onClick={onHide} className="btn btn-light btn-elevate">
            Cancel
          </button>
          <> </>
          <button type="button" onClick={onHide} className="btn btn-primary btn-elevate">
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
