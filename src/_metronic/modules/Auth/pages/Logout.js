import React from 'react';
import { Redirect } from 'react-router-dom';

const Logout = () => {
  return <Redirect to="/auth/login" />;
};

export default Logout;
