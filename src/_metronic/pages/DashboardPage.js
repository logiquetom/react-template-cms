import React from 'react';
import { Dashboard } from '@metronic/_partials';

export function DashboardPage() {
  return <Dashboard />;
}
