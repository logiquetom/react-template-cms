import React, { memo } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const LoadingCircular = () => {
  return (
    <div
      className="loading-container"
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
      }}
    >
      <CircularProgress className="loading-circular" />
    </div>
  );
};

export default memo(LoadingCircular);
