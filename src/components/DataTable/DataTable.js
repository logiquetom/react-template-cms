import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useHistory } from 'react-router-dom';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineSharpIcon from '@material-ui/icons/DeleteOutlineSharp';
import getObjectValue from '@helpers/getObjectValue.js';
import { styDelete, styRow } from './styles.module.scss';

const DataTable = props => {
  const {
    className,
    headers,
    detailLink,
    updateLink,
    deleteRow,
    rows,
    renderFunctions,
    sortByColumnName,
    sortByDirection,
    sortFunction,
    Pagination,
    isFetching,
    ...rest
  } = props;
  const history = useHistory();

  const renderActions = row => {
    return (
      <div className="d-flex align-items-center">
        {updateLink && (
          <div className="cursor-pointer mr-3" onClick={() => history.push(`${updateLink}${row.id}`, { detail: row })}>
            <EditOutlinedIcon />
          </div>
        )}
        {deleteRow && (
          <div className={styDelete} onClick={() => deleteRow(row)}>
            <DeleteOutlineSharpIcon />
          </div>
        )}
      </div>
    );
  };
  const viewDetail = row => {
    if (detailLink) history.push(`${detailLink}${row.id}`, { detail: row });
  };
  return (
    <div {...rest}>
      <PerfectScrollbar>
        <div>
          <Table size="medium" aria-label="a sticky table" stickyHeader>
            <TableHead>
              <TableRow>
                {headers.map(header => (
                  <TableCell key={header.key}>
                    {(header.sortable === undefined || header.sortable) && (
                      <TableSortLabel
                        active={sortByColumnName === header.key}
                        direction={sortByDirection.toLowerCase()}
                        onClick={() => sortFunction(header.key, sortByDirection === 'ASC' ? 'DESC' : 'ASC')}
                      >
                        {header.displayName}
                      </TableSortLabel>
                    )}
                    {header.sortable !== undefined && !header.sortable && <>{header.displayName} </>}
                  </TableCell>
                ))}
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow className={styRow} hover key={row.id}>
                  {headers.map(header => {
                    return header.type !== 'image' ? (
                      <TableCell key={header.key} onClick={() => viewDetail(row)}>
                        {renderFunctions && renderFunctions[header.key]
                          ? renderFunctions[header.key](row)
                          : getObjectValue(row, header.key)}
                      </TableCell>
                    ) : (
                      <TableCell key={header.key} onClick={() => viewDetail(row)}>
                        <img
                          src={getObjectValue(row, header.key)}
                          style={{ maxWidth: '150px', height: 'auto', objectFit: 'contain' }}
                          alt="Empty"
                        />
                      </TableCell>
                    );
                  })}
                  <TableCell>{renderActions(row)}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {!isFetching && rows.length === 0 && (
            <div className="d-flex align-items-center justify-content-center my-5">Empty List</div>
          )}
        </div>
      </PerfectScrollbar>
      {rows.length > 0 && Pagination}
    </div>
  );
};

DataTable.defaultProps = {
  className: null,
  deleteRow: null,
  detailLink: '',
  isFetching: false,
  rows: [],
  renderFunctions: {},
  sortByColumnName: 'id',
  sortByDirection: 'DESC',
  sortFunction: null,
  Pagination: null,
};

DataTable.propTypes = {
  className: PropTypes.string,
  deleteRow: PropTypes.func,
  detailLink: PropTypes.string,
  headers: PropTypes.arrayOf(PropTypes.object).isRequired,
  isFetching: PropTypes.bool,
  Pagination: PropTypes.node,
  renderFunctions: PropTypes.object,
  rows: PropTypes.arrayOf(PropTypes.object),
  sortByColumnName: PropTypes.string,
  sortByDirection: PropTypes.string,
  sortFunction: PropTypes.func,
  updateLink: PropTypes.string.isRequired,
};

export default DataTable;
