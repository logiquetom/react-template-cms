import loadable from '@loadable/component';

const DataTable = loadable(() => import(/* webpackChunkName: "data-table" */ './DataTable'), {
  fallback: null,
});

const TableFilter = loadable(() => import(/* webpackChunkName: "table-filter" */ './TableFilter'), {
  fallback: null,
});

export { DataTable, TableFilter };
