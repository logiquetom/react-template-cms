import React, { useState } from 'react';
import { string, func, arrayOf, node } from 'prop-types';
import { styFilter, hasMoreFilter } from './styles.module.scss';
import { Textfield } from '@components/Textfield';
import useDebounceCallback from '@hooks/useDebounceCallback';

const TableFilter = ({ onSearch, placeholder, onClear, moreFilters }) => {
  const [searchText, setSearchText] = useState('');
  const debouncedOnSearch = useDebounceCallback(value => onSearch(value), 500);
  const handleSearch = e => {
    const { value } = e.target;
    setSearchText(value || '');

    debouncedOnSearch(value);
  };
  const handleClear = () => {
    onClear();
    setSearchText('');
  };
  return (
    <div className="d-flex flex-row">
      <div
        className={`d-flex flex-row ${styFilter} col-10`}
        style={{ justifyContent: moreFilters.length > 2 ? 'flex-start' : 'flex-end' }}
      >
        {moreFilters.map(node => node)}
        <Textfield placeholder={placeholder} onChange={handleSearch} name="search" label="Search" value={searchText} />
      </div>
      <div className={`d-flex flex-row col-2 ${moreFilters.length > 2 ? hasMoreFilter + ' ml-3' : ''}`}>
        <button
          type="button"
          className={`btn btn-outline-dark font-weight-bold px-9 py-2 w-100 ${moreFilters.length > 2 && 'ml-5'}`}
          onClick={handleClear}
        >
          Clear
        </button>
      </div>
    </div>
  );
};
TableFilter.defaultProps = {
  placeholder: 'Search...',
  moreFilters: [],
};
TableFilter.propTypes = {
  moreFilters: arrayOf(node),
  placeholder: string,
  onClear: func.isRequired,
  onSearch: func.isRequired,
};
export default TableFilter;
