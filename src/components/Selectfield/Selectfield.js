import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  root: {
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'black',
    },
    '& .MuiInputBase-root.Mui-disabled': {
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
});
const SelectField = props => {
  const { className, name, label, items, ...rest } = props;
  const classes = useStyles();
  return (
    <TextField
      classes={{
        root: classes.root,
      }}
      label={label}
      name={name}
      select
      className={className}
      {...rest}
    >
      {items.map((item, index) => {
        return (
          <MenuItem value={item.value} key={index}>
            {item.render ? item.render : item.label}
          </MenuItem>
        );
      })}
    </TextField>
  );
};

SelectField.defaultProps = {
  className: null,
};

SelectField.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ).isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default SelectField;
