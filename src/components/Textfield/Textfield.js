import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Textfield from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  root: {
    '& .MuiInput-root': {
      height: '100%',
    },
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'black',
    },
    '& .MuiInputBase-root.Mui-disabled': {
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
});
const TextField = props => {
  const { className, name, label, ...rest } = props;
  const classes = useStyles();
  const x = '1';
  return (
    <Textfield
      classes={{
        root: classes.root,
      }}
      label={label}
      name={name}
      className={className}
      {...rest}
    />
  );
};

TextField.defaultProps = {
  className: null,
};

TextField.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default TextField;
