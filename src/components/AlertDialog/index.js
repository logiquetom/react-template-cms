import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { bool, func, object, string } from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/styles';
const styles = {
  dialogPaper: {
    padding: '15px 30px',
  },
};
const AlertDialog = props => {
  const { display, closeDialog, onConfirm, loading, classes, title, description, action } = props;
  return (
    <Dialog
      open={display}
      onClose={closeDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      classes={{
        paper: classes.dialogPaper,
      }}
    >
      {loading ? (
        <div className="d-flex justify-content-center align-items-center flex-column">
          <CircularProgress color="secondary" />
        </div>
      ) : (
        <>
          <DialogContent className="mb-8">
            <div className="d-flex justify-content-center align-items-center flex-column">
              <div className="h1 text-dark mb-4">{title}</div>
              <div className="font-size-md text-dark-50 font-weight-bold">{description}</div>
            </div>
          </DialogContent>
          <DialogActions className="d-flex justify-content-center">
            <button type="button" className="btn btn-dark w-50" onClick={onConfirm} color="primary" autoFocus>
              {action}
            </button>
            <button type="button" className="btn btn-outline-dark w-50" onClick={closeDialog} color="primary">
              Cancel
            </button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};
AlertDialog.defaultProps = {
  action: 'Confirm',
  display: false,
  loading: false,
  classes: null,
};
AlertDialog.propTypes = {
  action: string,
  classes: object,
  closeDialog: func.isRequired,
  description: string.isRequired,
  display: bool,
  loading: bool,
  title: string.isRequired,
  onConfirm: func.isRequired,
};
export default withStyles(styles)(AlertDialog);
