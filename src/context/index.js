import React from 'react';
import { node } from 'prop-types';
import { LocaleProvider } from '@context/locale';
import { ToasterProvider } from '@context/toaster';

function ContextProvider({ children }) {
  return (
    <LocaleProvider>
      <ToasterProvider>{children}</ToasterProvider>
    </LocaleProvider>
  );
}

ContextProvider.propTypes = {
  children: node.isRequired,
};

export default ContextProvider;
