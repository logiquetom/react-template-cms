import React from 'react';
import { node } from 'prop-types';
import { useCookies } from 'react-cookie';
import { useQuery } from 'react-query';
import Loading from '@components/LoadingCircular';
import useFetch from '@hooks/useFetch';
import { RESTAPI_HOST, AUTH_TOKEN, BASE_URL } from '@constants';

export const AuthContext = React.createContext({
  handleLogout: () => {},
  token: '',
  user: {},
});

export const AuthProvider = props => {
  const { children } = props;
  const [cookies, , removeCookie] = useCookies([AUTH_TOKEN]);
  const { data, status, error } = useQuery(['userDetail'], useFetch(`${RESTAPI_HOST}/me`));

  const token = cookies[AUTH_TOKEN];
  if (status === 'loading') {
    return <Loading />;
  }

  if (error) {
    window.location.href = `${BASE_URL}/login`;

    return null;
  }

  const handleLogout = () => {
    removeCookie(AUTH_TOKEN, {
      path: '/',
    });

    window.location.href = `${BASE_URL}/login`;
  };

  return (
    <AuthContext.Provider value={{ user: data?.data || {}, handleLogout, token }}>{children}</AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: node.isRequired,
};

export default AuthProvider;
