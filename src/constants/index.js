import transformConstants from './transform';

const transformedClientConstants = transformConstants(process.env);

const constants = {
  ...transformedClientConstants,
  AUTH_TOKEN: 'AUTH_TOKEN',
  BASE_URL: process.env.PUBLIC_URL ? new URL(process.env.PUBLIC_URL).pathname : '',
};

export const { RESTAPI_HOST, GQL_HOST, TRANSACTION_ID, RESTAPI_KEY, AUTH_TOKEN, BASE_URL } = constants;
