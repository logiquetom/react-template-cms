// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider } from 'react-bootstrap-table2-paginator';
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
  headerSortingClasses,
} from '@metronic/_helpers';
import { Pagination } from '@metronic/_partials/controls';
import * as uiHelpers from '../CustomersUIHelpers';
import * as columnFormatters from './column-formatters';
import { useCustomersUIContext } from '../CustomersUIContext';

const customers = [
  {
    id: 12,
    firstName: 'Sonni',
    lastName: 'Gabotti',
    email: 'sgabotti0@wsj.com',
    userName: 'sgabotti0',
    gender: 'Female',
    status: 0,
    dateOfBbirth: '10/14/1950',
    ipAddress: '251.237.126.210',
    type: 1,
    _userId: 1,
    _createdDate: '09/07/2016',
    _updatedDate: '05/31/2013',
  },
  {
    id: 21891,
    firstName: 'Sonni',
    lastName: 'Gabotti',
    email: 'sgabotti0@wsj.com',
    userName: 'sgabotti0',
    gender: 'Female',
    status: 0,
    dateOfBbirth: '10/14/1950',
    ipAddress: '251.237.126.210',
    type: 1,
    _userId: 1,
    _createdDate: '09/07/2016',
    _updatedDate: '05/31/2013',
  },
  {
    id: 12323123,
    firstName: 'Sonni',
    lastName: 'Gabotti',
    email: 'sgabotti0@wsj.com',
    userName: 'sgabotti0',
    gender: 'Female',
    status: 0,
    dateOfBbirth: '10/14/1950',
    ipAddress: '251.237.126.210',
    type: 1,
    _userId: 1,
    _createdDate: '09/07/2016',
    _updatedDate: '05/31/2013',
  },
  {
    id: 5,
    firstName: 'Sonni',
    lastName: 'Gabotti',
    email: 'sgabotti0@wsj.com',
    userName: 'sgabotti0',
    gender: 'Female',
    status: 0,
    dateOfBbirth: '10/14/1950',
    ipAddress: '251.237.126.210',
    type: 1,
    _userId: 1,
    _createdDate: '09/07/2016',
    _updatedDate: '05/31/2013',
  },
];

export function CustomersTable() {
  // Customers UI Context
  const customersUIContext = useCustomersUIContext();
  const customersUIProps = useMemo(() => {
    return {
      ids: customersUIContext.ids,
      setIds: customersUIContext.setIds,
      queryParams: customersUIContext.queryParams,
      setQueryParams: customersUIContext.setQueryParams,
      openEditCustomerDialog: customersUIContext.openEditCustomerDialog,
      openDeleteCustomerDialog: customersUIContext.openDeleteCustomerDialog,
    };
  }, [customersUIContext]);

  useEffect(() => {
    // clear selections list
    customersUIProps.setIds([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customersUIProps.queryParams]);
  // Table columns
  const columns = [
    {
      dataField: 'id',
      text: 'ID',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'firstName',
      text: 'Firstname',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'lastName',
      text: 'Lastname',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'email',
      text: 'Email',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'gender',
      text: 'Gender',
      sort: false,
      sortCaret: sortCaret,
    },
    {
      dataField: 'status',
      text: 'Status',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: 'type',
      text: 'Type',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.TypeColumnFormatter,
    },
    {
      dataField: 'action',
      text: 'Actions',
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditCustomerDialog: customersUIProps.openEditCustomerDialog,
        openDeleteCustomerDialog: customersUIProps.openDeleteCustomerDialog,
      },
      classes: 'text-right pr-0',
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px',
      },
    },
  ];
  const entities = customers;
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: customers.count,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: customersUIProps.queryParams.pageSize,
    page: customersUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={false} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                bordered={false}
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(customersUIProps.setQueryParams)}
                selectRow={getSelectRow({
                  entities,
                  ids: customersUIProps.ids,
                  setIds: customersUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
