import React from 'react';
import { LoadingDialog } from '@metronic/_partials/controls';

export function CustomersLoadingDialog() {
  // looking for loading/dispatch
  return <LoadingDialog isLoading={false} text="Loading ..." />;
}
