import React, { useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider } from 'react-bootstrap-table2-paginator';
import * as uiHelpers from '../ProductsUIHelpers';
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from '@metronic/_helpers';
import * as columnFormatters from './column-formatters';
import { Pagination } from '@metronic/_partials/controls';
import { useProductsUIContext } from '../ProductsUIContext';

const products = [
  {
    id: 226,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 1,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 20982196,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 256,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 23126,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
  {
    id: 216,
    model: 'Rio',
    manufacture: 'Kia',
    modelYear: 2007,
    mileage: 189651,
    description:
      "The Kia Opirus was an executive car manufactured and marketed by Kia Motors that was launched in April 2003 and was marketed globally under various nameplates, prominently as the Amanti. It was considered to be Kia's flagship vehicle.",
    color: 'Indigo',
    price: 44292,
    condition: 1,
    createdDate: '09/01/2017',
    status: 1,
    VINCode: '1C4SDHCT2CC055294',
    _userId: 2,
    _createdDate: '04/09/2018',
    _updatedDate: '04/17/2014',
  },
];

export function ProductsTable() {
  // Products UI Context
  const productsUIContext = useProductsUIContext();
  const productsUIProps = useMemo(() => {
    return {
      ids: productsUIContext.ids,
      setIds: productsUIContext.setIds,
      queryParams: productsUIContext.queryParams,
      setQueryParams: productsUIContext.setQueryParams,
      openEditProductPage: productsUIContext.openEditProductPage,
      openDeleteProductDialog: productsUIContext.openDeleteProductDialog,
    };
  }, [productsUIContext]);

  // Table columns
  const columns = [
    {
      dataField: 'VINCode',
      text: 'VIN Code (ID)',
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: 'manufacture',
      text: 'Manufacture',
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: 'model',
      text: 'Model',
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: 'modelYear',
      text: 'Model Year',
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: 'color',
      text: 'Color',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: 'price',
      text: 'Price',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: 'status',
      text: 'Status',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: 'condition',
      text: 'Condition',
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: 'action',
      text: 'Actions',
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditProductPage: productsUIProps.openEditProductPage,
        openDeleteProductDialog: productsUIProps.openDeleteProductDialog,
      },
      classes: 'text-right pr-0',
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px',
      },
    },
  ];
  const entities = products;
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: products.count,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: productsUIProps.queryParams.pageSize,
    page: productsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={false} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(productsUIProps.setQueryParams)}
                selectRow={getSelectRow({
                  entities,
                  ids: productsUIProps.ids,
                  setIds: productsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
