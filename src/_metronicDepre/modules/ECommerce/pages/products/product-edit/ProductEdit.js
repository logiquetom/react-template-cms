/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from 'react';
import { Card, CardBody, CardHeader, CardHeaderToolbar } from '@metronic/_partials/controls';
import { ProductEditForm } from './ProductEditForm';
import { useSubheader } from '@metronic/layout';
import { ModalProgressBar } from '@metronic/_partials/controls';

const initProduct = {
  id: undefined,
  model: '',
  manufacture: 'Pontiac',
  modelYear: 2020,
  mileage: 0,
  description: '',
  color: 'Red',
  price: 10000,
  condition: 1,
  status: 0,
  VINCode: '',
};

const productForEdit = initProduct;

export function ProductEdit({
  history,
  match: {
    params: { id },
  },
}) {
  const actionsLoading = false;
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState('basic');
  const [title, setTitle] = useState('');

  useEffect(() => {
    let _title = id ? '' : 'New Product';
    if (productForEdit && id) {
      _title = `Edit product '${productForEdit.manufacture} ${productForEdit.model} - ${productForEdit.modelYear}'`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productForEdit, id]);

  const btnRef = useRef();
  const saveProductClick = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backToProductsList = () => {
    history.push(`/e-commerce/products`);
  };

  return (
    <Card>
      {actionsLoading && <ModalProgressBar />}
      <CardHeader title={title}>
        <CardHeaderToolbar>
          <button type="button" onClick={backToProductsList} className="btn btn-light">
            <i className="fa fa-arrow-left"></i>
            Back
          </button>
          {`  `}
          <button type="button" className="btn btn-light ml-2">
            <i className="fa fa-redo"></i>
            Reset
          </button>
          {`  `}
          <button type="submit" className="btn btn-primary ml-2" onClick={saveProductClick}>
            Save
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ul className="nav nav-tabs nav-tabs-line " role="tablist">
          <li className="nav-item" onClick={() => setTab('basic')}>
            <a
              className={`nav-link ${tab === 'basic' && 'active'}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === 'basic').toString()}
            >
              Basic info
            </a>
          </li>
        </ul>
        <div className="mt-5">
          {tab === 'basic' && (
            <ProductEditForm
              actionsLoading={false}
              product={initProduct}
              btnRef={btnRef}
              saveProduct={() => {
                setTimeout(() => {
                  backToProductsList();
                }, 1000);
              }}
            />
          )}
        </div>
      </CardBody>
    </Card>
  );
}
