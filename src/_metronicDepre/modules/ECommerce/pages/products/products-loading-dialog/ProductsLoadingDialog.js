import React from 'react';
import { LoadingDialog } from '@metronic/_partials/controls';

export function ProductsLoadingDialog() {
  return <LoadingDialog isLoading={false} text="Loading ..." />;
}
