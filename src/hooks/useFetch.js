import { useCookies } from 'react-cookie';
import { AUTH_TOKEN } from '@constants';

/**
 * Hooks to generate fetch interface for react-query library
 * @param {string|Function} urlScheme scheme to generate url, can be string or function
 * @param {object} options Options for fetch API
 *
 * @returns {Promise} Returns fetch interface to be consumed by react-query library
 *
 * @example
 * import { useQuery } from 'react-query';
 * import useFetch from '@hooks/useFetch';
 *
 * const { data, status, error } = useQuery(['userDetail'], useFetch('/user'));
 * -- or --
 * const { data, status, error } = useQuery(['userDetail', { userID: 1 }], useFetch(({ userID }) => `/user/${userID}`));
 */

const MUTATION_REQUESTS = ['POST', 'PUT'];

const useFetch = (urlScheme, options = {}) => {
  const [cookies] = useCookies([AUTH_TOKEN]);
  const token = cookies[AUTH_TOKEN];

  return (key, variables) => {
    const controller = new AbortController();
    const signal = controller.signal;

    const usedVariables = MUTATION_REQUESTS.includes(options.method) ? key : variables;

    let finalUrl = String(urlScheme);

    if (usedVariables && typeof urlScheme === 'function') {
      finalUrl = urlScheme(usedVariables);
    }

    const promise = fetch(finalUrl, {
      signal,
      ...(MUTATION_REQUESTS.includes(options.method) && { body: JSON.stringify(usedVariables) }),
      ...options,
      headers: {
        'Content-Type': 'application/json',
        ...(token && { token }),
        ...(options.headers || {}),
      },
    })
      .then(res => res.json())
      .catch(error => {
        if (window.__clientLogger) {
          let originalUrl = finalUrl;

          try {
            const { origin, pathname } = new URL(finalUrl);
            originalUrl = `${origin}${pathname}`;
          } catch {
            // do nothing
          }

          window.__clientLogger.notify({
            error: {
              message: `Error occurred for API: ${originalUrl}`,
              name: '[DHC Admin]',
            },
            params: {
              error,
              apiUrl: finalUrl,
              apiToken: token,
              variables: usedVariables,
            },
          });
        }
      });

    promise.cancel = controller.abort;

    return promise;
  };
};

export default useFetch;
