import { useEffect, useRef } from 'react';

function useDebouncedCallback(callback, delay) {
  const argsRef = useRef();
  const previousArgs = useRef();
  const timeout = useRef();

  function cleanup() {
    if (timeout.current) {
      clearTimeout(timeout.current);
    }
  }
  useEffect(() => cleanup, []);

  return function debouncedCallback(args) {
    argsRef.current = args;

    cleanup();

    timeout.current = setTimeout(() => {
      if (previousArgs.current !== args) {
        callback(argsRef.current);
        previousArgs.current = args;
      }
    }, delay);
  };
}
export default useDebouncedCallback;
