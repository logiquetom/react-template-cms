export default function getObjectValue(object, path = '') {
  return path.split('.').reduce((o, x) => (o === undefined ? o : o[x]), object);
}
