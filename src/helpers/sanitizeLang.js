export default function sanitizeLang(lang) {
  return /^(id|en)$/.test(lang) ? lang : 'id';
}
