/* eslint-disable no-template-curly-in-string */
module.exports = {
  presets: ['react-app'],
  plugins: [
    'lodash',
    'graphql-tag',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    [
      'transform-imports',
      {
        reactstrap: {
          transform: 'reactstrap/lib/${member}',
          preventFullImport: true,
        },
      },
    ],
  ],
  env: {
    production: {
      plugins: [
        [
          'transform-react-remove-prop-types',
          {
            mode: 'remove',
            removeImport: true,
            additionalLibraries: ['prop-types-extra'],
            ignoreFilenames: ['node_modules'],
          },
        ],
      ],
    },
  },
};
