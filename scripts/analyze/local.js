process.env.GENERATE_SOURCEMAP = false;

function analyzerLocal() {
  const webpack = require('webpack');
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
  const webpackConfigProd = require('../../config/webpack.config')('production');

  webpackConfigProd.plugins.push(new BundleAnalyzerPlugin());

  // actually running compilation and waiting for plugin to start explorer
  webpack(webpackConfigProd, (err, stats) => {
    if (err || stats.hasErrors()) {
      console.error(err);
    }
  });
}

module.exports = analyzerLocal;
