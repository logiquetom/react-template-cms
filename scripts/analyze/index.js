process.env.NODE_ENV = 'production';
process.env.SKIP_GENERATE_STATS = true;

(function() {
  const { NETLIFY, GITLAB_CI } = process.env;

  let analyzer;

  switch (true) {
    case Boolean(NETLIFY):
      // Use this when you are hosting in netlify
      analyzer = require('./netlify');
      break;
    case Boolean(GITLAB_CI):
      // Use this when you are self running the build in gitlab,
      // And upload the build folder together with the stats file to the CDN (such as now)
      analyzer = require('./gitlab');
      break;
    default:
      analyzer = require('./local');
      break;
  }

  analyzer();
})();
