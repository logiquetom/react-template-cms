/* eslint-disable no-eval */

async function analyzerGitlab() {
  const {
    ANALYZE_BUNDLE,
    CI_JOB_STAGE = '',
    PA_TOKEN_AUTH: GITLAB_PA_TOKEN,
    CI_PROJECT_ID,
    CI_MERGE_REQUEST_IID,
    CI_API_V4_URL,
    CI_PROJECT_NAME,
  } = process.env;

  // This should be used when stats.json file is present in build folder.
  const shouldAnalyze = Boolean(ANALYZE_BUNDLE || CI_JOB_STAGE === 'analyze');

  if (shouldAnalyze) {
    const fs = require('fs-extra');

    const { appBuild } = require('../../config/paths');

    const statsExists = await fs.pathExists(`${appBuild}/stats.json`);

    if (!statsExists) {
      console.error('[ERROR] There is no stats.json file found. Aborting job...');
      process.exit(1);
    }

    const { exec } = require('child_process');
    const { promisify } = require('util');

    const promisified = promisify(exec);

    await promisified(
      `webpack-bundle-analyzer ${appBuild}/stats.json -r ${appBuild}/bundle-analyze-${CI_MERGE_REQUEST_IID}.html -m static --no-open`,
    );

    if (!GITLAB_PA_TOKEN || !CI_PROJECT_NAME || !CI_PROJECT_ID || !CI_API_V4_URL || !CI_MERGE_REQUEST_IID) {
      console.error('[ERROR] Some of environment variables needed are missing. Aborting job...');
      process.exit(1);
    }

    try {
      const { stdout } = await promisified(
        `curl --request GET --header "PRIVATE-TOKEN: ${GITLAB_PA_TOKEN}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes`,
      );

      if (eval(stdout).find(note => note.body.includes(`[Bundle Analyzer]`))) {
        // eslint-disable-next-line
        console.info('[INFO] Bundle analyzer note already exists. Nothing else to do.');
        process.exit(0);
      }

      const bundleAnalyzeLink = `https://deploy-preview-${CI_MERGE_REQUEST_IID}--netlify-url.com/bundle-analyze-${CI_MERGE_REQUEST_IID}.html`;

      const body = encodeURIComponent(
        `[Bundle Analyzer]\n\nOpen the generated report here: [${bundleAnalyzeLink}](${bundleAnalyzeLink}).\n\nThis message was auto generated on ${new Date(
          Date.now(),
        ).toLocaleString('id-ID', { timeZone: 'Asia/Jakarta' })}.`,
      );

      await promisified(
        `curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_PA_TOKEN}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes?body="${body}"`,
      );
    } catch (error) {
      console.error('[ERROR] Error when posting to gitlab note: ', error);
    }

    return;
  }
}

module.exports = analyzerGitlab;
