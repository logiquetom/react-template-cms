/* eslint-disable no-eval */

async function analyzerNetlify() {
  const {
    /* Gitlab related variables, need to hardcode */
    GITLAB_PA_TOKEN = '6a4HAXGXtsjcEZdbYB4Z',
    CI_PROJECT_ID = '', // replace this with your gitlab's project id
    CI_API_V4_URL = 'https://gitlab.com/api/v4',

    /* Netlify predefined variables */
    REVIEW_ID,
    DEPLOY_PRIME_URL,
    // COMMIT_REF, for now we won't be using this until we support delete gitlab note
    CONTEXT,
  } = process.env;
  if (CONTEXT !== 'deploy-preview') {
    console.info('[INFO] Skipping analyzer script because it is not a deploy preview.');
    return;
  }

  const fs = require('fs-extra');

  const { appBuild } = require('../../config/paths');

  const statsExists = await fs.pathExists(`${appBuild}/stats.json`);

  if (!statsExists) {
    console.error('[ERROR] There is no stats.json file found. Aborting job...');
    process.exit(1);
  }

  const { exec } = require('child_process');
  const { promisify } = require('util');

  const promisified = promisify(exec);

  await promisified(
    `webpack-bundle-analyzer ${appBuild}/stats.json -r ${appBuild}/bundle-analyze-${REVIEW_ID}.html -m static --no-open`,
  );

  if (!GITLAB_PA_TOKEN || !CI_PROJECT_ID || !CI_API_V4_URL || !REVIEW_ID) {
    console.error('[ERROR] Some of environment variables needed are missing. Aborting job...');
    process.exit(1);
  }

  try {
    const { stdout } = await promisified(
      `curl --request GET --header "PRIVATE-TOKEN: ${GITLAB_PA_TOKEN}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${REVIEW_ID}/notes`,
    );

    if (eval(stdout).find(note => note.body.includes(`[Bundle Analyzer]`))) {
      console.info('[INFO] Bundle analyzer note already exists. Nothing else to do.');
      process.exit(0);
    }

    const bundleAnalyzeLink = `${DEPLOY_PRIME_URL}/bundle-analyze-${REVIEW_ID}.html`;

    const body = encodeURIComponent(
      `[Bundle Analyzer]\n\nOpen the generated report here: [${bundleAnalyzeLink}](${bundleAnalyzeLink}).\n\nThis message was auto generated on ${new Date(
        Date.now(),
      ).toLocaleString('id-ID', { timeZone: 'Asia/Jakarta' })}.`,
    );

    await promisified(
      `curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_PA_TOKEN}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${REVIEW_ID}/notes?body="${body}"`,
    );
  } catch (error) {
    console.error('[ERROR] Error when posting to gitlab note: ', error);
  }
}

module.exports = analyzerNetlify;
