const fs = require('fs');
const paths = require('../config/paths');
const exec = require('./utils/verboseExec');

const { TARGET_ENV = 'production' } = process.env;

if (!fs.existsSync(paths.dotenv)) {
  exec(`cp ${paths.appEnvironment}/.env.${TARGET_ENV} .env`);
}

let cmd = '';

switch (TARGET_ENV) {
  case 'development':
    cmd = '-dev';
    break;
  case 'staging':
    cmd = '-staging';
    break;
  default:
    break;
}

exec(`yarn deploy${cmd}`);
