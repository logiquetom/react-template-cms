const baseConfig = require('./config/jest/base/react');
const resolvers = require('./config/jest/resolvers/react')();
const transformers = require('./config/jest/transformers/react')();

module.exports = {
  ...baseConfig,
  projects: [
    {
      displayName: 'Test Client',
      testMatch: ['<rootDir>/src/**/__tests__/**/*.js'],
      testEnvironment: 'jsdom',
      moduleNameMapper: resolvers,
      transform: transformers,
      setupFilesAfterEnv: ['./config/jest/setup/react/afterEnv'],
    },
  ],
};
